from six.moves import cPickle as pickle
def load_dict(filename_):
    print(filename_)
    with open(filename_, 'rb') as f:
        ret_di = pickle.load(f)
        #ret_di = joblib.load(f)
    return ret_di

def save_dict(di_, filename_):
    with open(filename_, 'wb') as f:
        pickle.dump(di_, f, protocol=4)
        #joblib.dump(di_, f)

from bs4 import BeautifulSoup
import requests 
import csv
import feedparser

def fetchRSSfeedfromPage(category,URL='https://blog.feedspot.com/world_news_rss_feeds/'):
    
    r = requests.get(URL) 
    soup = BeautifulSoup(r.content, 'html5lib')
    data_items = soup.findAll('div',attrs = {'class':'data'})
    with open('config/feedConfig_'+category+'.csv', "w+",newline='\n', encoding='utf-8') as csvfile:
        fieldnames = ['source', 'feed', 'location', 'type']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for data in data_items:
            location=data.find('p',attrs = {'class':'loc'})

            feed=data.a['href'].split('://')[1]
            if feed.split('.')[0]=='www' or feed.split('.')[0]=='rss' or feed.split('.')[0]=='feeds':
                source=feed.split('.')[1]
            else:
                source=feed.split('.')[0]
            #print(source, feed,location.text,'World News')
            try: 
                #print (source,feed,getLocation(location),category)
                writer.writerow({'source': source, 'feed': data.a['href'], 'location': getLocation(location) , 'type': category})
            except AttributeError as e:
                print(feed,location)
                print(e)
                pass      
        
def getLocation(location):
    if location==None:
        return 'Global'
    else:
        return location.text

def initialize():
    with open('config/feedConfig.csv', "w",newline='\n', encoding='utf-8') as csvfile:
        fieldnames = ['source', 'feed', 'location', 'type']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

def getValues(entry,source,source_type):
    item={}
    tags=[]
    
    if 'published' in entry:
        item['published_date']=entry.published
    else:
        item['published_date']=entry.updated
    item['title']=entry.title
    if '/>' in entry.summary:
        entry_summary =  entry.summary.split('/>')[1]
    else:
        entry_summary = entry.summary
    item['summary']=entry_summary
    item['content_url']=entry.link
    if 'media_content' in entry.keys():
        item['image_url']=entry.media_content[0]['url']
    else: 
        item['image_url']=''
    if 'tags' in entry.keys():
        for tag in entry.tags:
            tags.append(tag.term)
    else:
        tags=''
    item['tags']=str(tags)
    if 'author' in entry.keys():
        item['author']=entry.author
    else:
        item['author']=''
    
    return item

import pandas as pd

import json
def convert_to_json(source,category):
    temp_dict=load_dict('data/'+category+'/dict/'+source+'_'+category+".pkl")
    with open('data/'+category+'/json/'+source+'_'+category+".json", 'w+') as outfile:
        json.dump(temp_dict,outfile)

def getRSSFeed(category):
    configDf=pd.read_csv("config/feedConfig_"+category+".csv")
    for index, row in configDf.iterrows():
        #print(row['feed'])
        try:
            NewsFeed = feedparser.parse(row['feed'])
        except Exception as e:
            print(e)
            pass
        feedsdict={}
        try:
            feedsdict=load_dict('data/'+category+'/dict/'+row['source']+'_'+row['type']+'.pkl')
        except Exception as e:
            print(e)
            pass

        for entry in NewsFeed.entries:
            try:
                item=getValues(entry,row['source'],row['type'])
                feedsdict[item['published_date']]= { 'title': item['title'], 'summary': item['summary'], 'tags': item['tags'], \
                                                           'content_url': item['content_url'], 'image_url': item['image_url'], \
                                                          'author':  item['author'], 'published_date': item['published_date']  }
            except Exception as e:
                print(e)
                print(entry)
                pass
        save_dict(feedsdict,'data/'+category+'/dict/'+row['source']+'_'+row['type']+'.pkl')
        convert_to_json(row['source'],category)
    
    return 

from calendar import monthrange

tweetDict={}

def checkTweetFile(category,source,month,year):
    import os.path
    if os.path.isfile('data/'+category+'/tweet/'+source+'_'+category+'_'+year+month+'.pkl'):
        return True
    else:
        return False

from numpy import nan
import time
import random
def getTwitterFeed(category,month='all',year='all'):
    configDf=pd.read_csv("config/tweetConfig_"+category+".csv")
    for index, row in configDf.iterrows():
        print(row['twitterhandle'])

        if (row['twitterhandle']==nan):
            print ("Not a valid twitter handle")
        else:
            if month=='all':
                monthlist=[1,2,3,4,5,6,7,8,9,10,11,12] 
            else:
                monthlist=month.split(',') 
            if year=='all':
                yearlist=[2015,2016,2017,2018,2019,2020]
            else:
                yearlist=year.split(',')
            for year in yearlist:
                for month in monthlist:
                    global tweetDict
                    if len(str(month))<2:
                        monthstr='0'+str(month)
                    else:
                        monthstr=str(month)
                    
                    fromDate=str(year)+'-'+monthstr+'-01'
                    toDate=str(year)+'-'+monthstr+'-'+str(monthrange(int(year),int(month))[1])
                    print(fromDate,toDate)
                
                    #call the setTwitterCriteria
                    if checkTweetFile(category,row['source'],monthstr,str(year)):

                        print ("File already presentt for :"+category+":"+row['source']+'_'+category+'_'+str(year)+monthstr+'.pkl')
                    else:
                        setTwitterCriteria(row['twitterhandle'],fromDate,toDate)
                        save_dict(tweetDict,'data/'+category+'/tweet/'+row['source']+'_'+category+'_'+str(year)+monthstr+'.pkl')     
                        tweetDict.clear()
                        print("File saved. Sleeping randomly between 100 to 200 sec to avoid IP ban from twitter")
                        time.sleep(random.randint(100,200))
    return True

def setTwitterCriteria(user,fromDate,toDate):
    import got3.manager
    import got3.models

    tweetCriteria = got3.manager.TweetCriteria().setUsername(user).setSince(fromDate).setUntil(toDate)
    got3.manager.TweetManager.getTweets(tweetCriteria,receiveBuffer)

import urllib.request
def getTwitterURLImageAndSummary(text):
    text_list=text.split('https')
    if (len(text_list)>1):
        url='https'+text_list[1]
    else:
        url='NA'
    if (len(text_list)>2):
        image_url='https'+text_list[2]
    else:
        image_url='NA'
        
        #url_data=urllib.request.urlopen(url).read())
    summary=text_list[0]
    return summary,url,image_url

import sys
def receiveBuffer(tweets):
    for t in tweets:
        global tweetDict
        summary,url,image_url=getTwitterURLImageAndSummary(t.text)
        tweetDict[t.date.strftime("%Y-%m-%d %H:%M")]={ 'id': t.id, 'summary': summary, 'tags': t.hashtags, \
                                                        'content_url': url, 'image_url': image_url, \
                                                        'published_date': t.date.strftime("%Y-%m-%d %H:%M"), 'retweets': t.retweets, 'favourites': t.favorites  }
        sys.stdout.write('.')
        sys.stdout.flush()
        
def createFolders():
    categories=['Economic','World','GeoPolitics','India','US','UK','Canada','AirForce','Army','Military','Business','StockMarket','Medicine','Science']
    for category in categories:
        os.mkdir("./data/"+category+'/dict/')
        os.mkdir("./data/"+category+'/json/')

from os import listdir
from os.path import isfile, join

def get_files(category,file_type):
    mypath='./data/'+category+'/'+file_type
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    return onlyfiles,mypath

def get_categories():
    return listdir('./data/')
