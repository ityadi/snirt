from utils import getRSSFeed,get_categories
from os import listdir
from os.path import isfile, join

categories=get_categories()
for category in categories:
    print("Entering "+category)
    getRSSFeed(category)
